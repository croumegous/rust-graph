use std::collections::{HashMap, HashSet, VecDeque};

#[derive(Clone, Debug)]
struct Vertex {
    id: String,
    properties: HashMap<String, String>,
}

#[derive(Clone, Debug)]
struct Edge {
    id: String,
    from: Vertex,
    to: Vertex,
}

#[derive(Clone, Debug)]
struct Graph {
    vertices: Vec<Vertex>,
    edges: Vec<Edge>,
}

impl Graph {
    fn add_vertex(&mut self, vertex: Vertex) {
        self.vertices.push(vertex);
    }

    fn add_edge(&mut self, edge: Edge) {
        self.edges.push(edge);
    }

    fn add_e<'a>(&'a mut self, edge_id: &'a str) -> EdgeBuilder {
        EdgeBuilder::new(self, edge_id)
    }

    fn get_vertex(&self) -> &Vec<Vertex> {
        &self.vertices
    }

    fn get_edges(&self) -> &Vec<Edge> {
        &self.edges
    }

    fn out_edges(&self, vertex_id: &str) -> Vec<Edge> {
        let mut found_edges = Vec::new();
        for edge in &self.edges {
            if edge.from.id == vertex_id {
                found_edges.push(edge.clone());
            }
        }

        found_edges
    }

    fn in_edges(&self, vertex_id: &str) -> Vec<Edge> {
        let mut found_edges = Vec::new();
        for edge in &self.edges {
            if edge.to.id == vertex_id {
                found_edges.push(edge.clone());
            }
        }

        found_edges
    }

    fn out_vertex(&self, vertex_id: &str) -> Vec<&Vertex> {
        let mut found_vertex = Vec::new();
        for edge in &self.edges {
            if edge.from.id == vertex_id {
                found_vertex.push(&edge.to);
            }
        }

        found_vertex
    }

    fn in_vertex(&self, vertex_id: &str) -> Vec<&Vertex> {
        let mut found_vertex = Vec::new();
        for edge in &self.edges {
            if edge.to.id == vertex_id {
                found_vertex.push(&edge.from);
            }
        }

        found_vertex
    }

    fn drop_vertex(&mut self, vertex_id: &str) {
        self.vertices.retain(|v| v.id != vertex_id);
        self.edges
            .retain(|e| e.from.id != vertex_id && e.to.id != vertex_id);
    }
}

trait Has<'a> {
    fn has(&'a self, property_name: &str, property_value: &str) -> Vec<&'a Vertex>;
}

impl<'a> Has<'a> for Vec<Vertex> {
    fn has(&'a self, property_name: &str, property_value: &str) -> Vec<&'a Vertex> {
        self.iter()
            .filter(move |vertex| {
                vertex.properties.get(property_name) == Some(&property_value.to_owned())
            })
            .collect()
    }
}

struct EdgeBuilder<'a> {
    graph: &'a mut Graph,
    label: &'a str,
    from: Option<&'a Vertex>,
    to: Option<&'a Vertex>,
}

impl<'a> EdgeBuilder<'a> {
    fn new(graph: &'a mut Graph, edge_label: &'a str) -> Self {
        Self {
            graph,
            label: edge_label,
            from: None,
            to: None,
        }
    }

    fn from(mut self, vertex: &'a Vertex) -> Self {
        self.from = Some(vertex);
        self.check_and_build_edge()
    }

    fn to(mut self, vertex: &'a Vertex) -> Self {
        self.to = Some(vertex);
        self.check_and_build_edge()
    }

    pub(self) fn check_and_build_edge(self) -> Self {
        if self.to.is_some() && self.from.is_some() {
            let edge = Edge {
                id: self.label.to_string(),
                from: self.from.unwrap().clone(),
                to: self.to.unwrap().clone(),
            };
            self.graph.add_edge(edge);
        }
        self
    }
}

fn basic_traversal(graph: &Graph, start_vertex: &Vertex) -> Vec<Vertex> {
    let mut visited = HashSet::new();
    let mut result = Vec::new();
    let mut stack = VecDeque::new();

    stack.push_back(start_vertex.clone());

    while let Some(vertex) = stack.pop_back() {
        if !visited.contains(&vertex.id) {
            visited.insert(vertex.id.clone());
            result.push(vertex.clone());

            for neighbor in graph.out_vertex(&vertex.id) {
                if !visited.contains(&neighbor.id) {
                    stack.push_front(neighbor.clone());
                }
            }
        }
    }

    result
}

fn recursive_traversal(graph: &Graph, start_vertex: &Vertex) -> Vec<Vertex> {
    let mut visited = HashSet::new();
    let mut result = Vec::new();

    fn dfs(
        graph: &Graph,
        vertex: &Vertex,
        visited: &mut HashSet<String>,
        result: &mut Vec<Vertex>,
    ) {
        visited.insert(vertex.id.clone());
        result.push(vertex.clone());

        for neighbor in graph.out_vertex(&vertex.id) {
            if !visited.contains(&neighbor.id) {
                dfs(graph, neighbor, visited, result);
            }
        }
    }

    dfs(graph, start_vertex, &mut visited, &mut result);
    result
}

fn main() {
    //create vertex and edges
    let vertex1 = Vertex {
        id: "1".to_string(),
        properties: HashMap::from([("weight".to_string(), "1".to_string())]),
    };
    let vertex2 = Vertex {
        id: "2".to_string(),
        properties: HashMap::from([("weight".to_string(), "2".to_string())]),
    };
    let vertex3 = Vertex {
        id: "3".to_string(),
        properties: HashMap::from([("weight".to_string(), "3".to_string())]),
    };

    let edge1 = Edge {
        id: "1".to_string(),
        from: vertex1.clone(),
        to: vertex2.clone(),
    };
    let edge2 = Edge {
        id: "2".to_string(),
        from: vertex2.clone(),
        to: vertex3.clone(),
    };

    //create graph
    let mut graph = Graph {
        vertices: vec![vertex1.clone(), vertex2.clone(), vertex3.clone()],
        edges: vec![edge1, edge2],
    };

    println!("CHECK GRAPH");
    println!("{:?}", graph.get_edges());
    println!("{:?}", graph.get_vertex());
    println!("{:?}", graph.get_vertex().has("weight", "1"));
    println!("{:?}", graph.out_edges("2"));
    println!("{:?}", graph.in_edges("2"));

    println!("ADD EDGE");
    graph.add_e("4").from(&vertex1).to(&vertex3);
    println!("{:?}", graph.get_edges());

    println!("TRAVERSAL");
    println!("{:?}", basic_traversal(&graph, &vertex1));
    println!("{:?}", recursive_traversal(&graph, &vertex1));

    println!("DROP VERTEX");
    graph.drop_vertex("2");
    println!("{:?}", graph.get_edges());
    println!("{:?}", graph.get_vertex());
}
